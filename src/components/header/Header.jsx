import React from "react";
import "./Header.scss";
import logo from "../../img/logo.png";

class Header extends React.Component {
  render() {
    return (
      <div>
        <div className="header">
          <img className="header__logo" src={logo} alt="planesales" />
          <h1 className="header__title">The Global Aircraft Marketplace</h1>
        </div>
      </div>
    );
  }
}

export default Header;
