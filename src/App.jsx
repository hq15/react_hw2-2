import React from "react";
import axios from "axios";
import Header from "./components/header/Header";
import ProductList from "./components/ProductList/ProductList";
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";
import modalTemplates from "./components/modal/modalTemplates.js";

let modalDeclaration = {};

const [modalWindowDeclarations] = Object.values(modalTemplates);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [],
      displayState: "none",
      classWrapper: "",
      modalWindowDeclarations: modalWindowDeclarations,
      classNameIcon: "favor-icon",
      productsList: [],
      favoritesList: [],
    };
  }

  componentDidMount() {
    axios("/airplane.json").then((res) => {
      this.setState({ cards: res.data });
    });
    if (!localStorage.getItem("cart")) localStorage.setItem("cart", "[]");
    if (!localStorage.getItem("favorites"))
      localStorage.setItem("favorites", "[]");
  }

  didMount = () => {
    this.setState({
      classWrapper: "wrapper",
    });
  };

  didUnmounted = () => {
    this.setState({
      classWrapper: "",
    });
  };

  closeModal = (e) => {
    if (e.target.classList.contains("wrapper")) {
      this.setState({
        displayState: "none",
      });
    }
  };

  openModal = (e) => {
    const modalID = e.target.dataset.modalId;
    modalDeclaration = this.state.modalWindowDeclarations.find(
      (item) => item.id === modalID
    );
    this.targetProd = e.target.id;
    this.setState({
      displayState: "open",
    });
  };

  closeModal = () => {
    this.setState({
      displayState: "none",
    });
  };

  addToCart = (e) => {
    this.setState({
      displayState: "none",
    });
    let cartProds = JSON.parse(localStorage.getItem("cart"));
    let newProds = cartProds.map((element) => element);
    newProds.push(this.targetProd);
    localStorage.setItem("cart", JSON.stringify(newProds));
  };

  addToFavorites = (id) => {
    const favoritesProducts = JSON.parse(localStorage.getItem("favorites"));
    let newProducts = favoritesProducts.map((id) => id);
    if (newProducts.includes(id)) {
      newProducts = newProducts.filter((item) => item !== id);
      localStorage.setItem("favorites", JSON.stringify(newProducts));
    } else {
      newProducts.push(id);
      localStorage.setItem("favorites", JSON.stringify(newProducts));
    }
    this.setState({
      favoritesList: newProducts,
    });
  };

  render() {
    const favoritesList = JSON.parse(localStorage.getItem("favorites"));
    const productsList = this.state.cards.map((element) => {
      favoritesList.includes(element.id)
        ? (element.isFavorites = true)
        : (element.isFavorites = false);
      return element;
    });

    const {
      className,
      id,
      header,
      closeButton,
      description,
      classNameButton,
      textButtonLeft,
      textButtonRight,
    } = modalDeclaration;

    const cards = productsList;

    return (
      <>
        <div
          className={this.state.classWrapper}
          onClick={this.closeModal}
        ></div>
        <Header />

        <ProductList
          products={cards}
          onClick={this.openModal}
          favorites={this.addToFavorites}
          classNameIcon={this.state.classNameIcon}
        />

        {this.state.displayState === "open" && (
          <Modal
            className={className}
            id={id}
            onClick={this.closeModal}
            show={this.didMount}
            hidden={this.didUnmounted}
            header={header}
            closeButton={closeButton}
            description={description}
            actions={
              <>
                <Button
                  className={classNameButton}
                  text={textButtonLeft}
                  onClick={this.addToCart}
                />
                <Button
                  className={classNameButton}
                  text={textButtonRight}
                  onClick={this.closeModal}
                />
              </>
            }
          />
        )}
      </>
    );
  }
}

export default App;
